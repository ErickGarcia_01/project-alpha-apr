from django.urls import path
from .views import list_projects_, project_details
from . import views

urlpatterns = [
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", project_details, name="show_project"),
    path("", list_projects_, name="list_projects"),
]
