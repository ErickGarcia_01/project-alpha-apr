from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect
from projects.views import project_details
from projects import views
from tasks.views import show_task


def redirect_to_home(request):
    return redirect("/projects/")


urlpatterns = [
    path("mine/", show_task, name="show_my_tasks"),
    path("tasks/", include("tasks.urls")),
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", project_details, name="show_project"),
    path("", redirect_to_home, name="home"),
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("accounts/", include("accounts.urls")),
]
