from django.urls import path
from .views import create_task, show_task

urlpatterns = [
    path("mine/", show_task, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
